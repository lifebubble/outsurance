﻿using System.Linq;
using CSVApp.Contract;

namespace CSVApp
{
    public class SomeDomain
    {        
        private readonly IPersistor _persistor = null;
        private readonly IIndividualRepository _repo = null;

        public SomeDomain(IIndividualRepository repo, IPersistor persistor)
        {            
            _persistor = persistor;
            _repo = repo;
        }

        public void ExportSortedNamesByFrequencyThenAlphabetically()
        {
            var indivudals = _repo.GetIndividuals();

            var lastNames = (from i in indivudals
                             group i by i.LastName into g
                             select new { Name = g.Key, Count = g.Count() });

            var firstNames = (from i in indivudals
                              group i by i.FirstName into g
                             select new { Name = g.Key, Count = g.Count() });

            var unionList = lastNames.Union(firstNames);

            var results = unionList.Select(x => new KeyValue { Name = x.Name, Count = x.Count  }).OrderByDescending(x => x.Count).ThenBy(x => x.Name).ToList();            

            foreach (var result in results)
            {
                _persistor.WriteLine($"{result.Name}, {result.Count}");
            }
        }

        public void ExportSortedStreetName()
        {
            var indivudals = _repo.GetIndividuals();
            var results = indivudals.Select(x => SplitValues(x.Address)).OrderBy(x => x.Name).Select(x => x.OriginalValue).ToList();
            foreach (var result in results)
            {
                _persistor.WriteLine(result);
            }
        }

        private StreetInformation SplitValues(string value)
        {
            var spaceIndex = value.IndexOf(' ');            
            var afterSpace = value.Substring(spaceIndex);

            return new StreetInformation
            {
                Name = afterSpace,
                OriginalValue = value
            };
        }
    }
}
