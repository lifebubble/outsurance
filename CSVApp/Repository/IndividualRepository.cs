﻿using CSVApp.Contract;
using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVApp.Repository
{
    public class IndividualRepository : IIndividualRepository
    {
        CSVReader _csvreader = null;

        public IndividualRepository()
        {
            _csvreader = new CSVReader("data.csv", new CsvFileDescription
            {
                SeparatorChar = ',',
                FirstLineHasColumnNames = true
            });
        }

        public IEnumerable<Individual> GetIndividuals()
        {
            return _csvreader.Read();
        }
    }
}
