﻿using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVApp
{
    public class CSVReader
    {
        private readonly CsvContext _csvContext = null;
        private readonly CsvFileDescription _csvFileDescription = null;
        private readonly string _filePath = null;

        public CSVReader(string filePath, CsvFileDescription csvFileDescription)
        {
            _csvContext = new CsvContext();
            _csvFileDescription = csvFileDescription;
            _filePath = filePath;
        }

        public IEnumerable<Individual> Read()
        {
            return _csvContext.Read<Individual>(_filePath, _csvFileDescription);
        }
    }
}
