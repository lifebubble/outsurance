﻿namespace CSVApp.Contract
{
    public interface IPersistor
    {
        void WriteLine(string line);
    }
}