﻿using System.Collections.Generic;

namespace CSVApp.Contract
{
    public interface IIndividualRepository
    {
        IEnumerable<Individual> GetIndividuals();
    }
}