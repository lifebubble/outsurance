﻿using CSVApp.Contract;
using CSVApp.Repository;
using LINQtoCSV;
using System;

namespace CSVApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Export File 1");
            using (var persistor = new TextFilePersistor("File1.txt"))
            {
                var domain = new SomeDomain(new IndividualRepository(), persistor);
                domain.ExportSortedNamesByFrequencyThenAlphabetically();
            }

            Console.WriteLine("Export File 2");
            using (var persistor = new TextFilePersistor("File2.txt"))
            {
                var domain = new SomeDomain(new IndividualRepository(), persistor);
                domain.ExportSortedStreetName();
            }

            Console.WriteLine("End");
            Console.ReadKey();            
        }
    }
}
