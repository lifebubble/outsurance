﻿using System;
using System.IO;

namespace CSVApp.Contract
{
    public class TextFilePersistor : IPersistor, IDisposable
    {
        private readonly StreamWriter sw;
        bool disposed = false;

        public TextFilePersistor(string filename)
        {
            sw = new StreamWriter(filename);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        public void WriteLine(string line)
        {
            sw.WriteLine(line);
        }
    }
}
