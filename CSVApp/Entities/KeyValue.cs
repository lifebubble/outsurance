﻿using System.Diagnostics;

namespace CSVApp
{
    [DebuggerDisplay("Name = {Name} Count = {Count}")]
    public class KeyValue
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
