﻿using System.Diagnostics;

namespace CSVApp
{
    [DebuggerDisplay("OriginalValue = {OriginalValue} Name = {Name}")]
    public class StreetInformation
    {
        public string Name { get; set; }
        public string OriginalValue { get; set; }
    }
}
