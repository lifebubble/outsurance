﻿using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVApp
{
    public class Individual
    {
        [CsvColumn(FieldIndex = 1)]
        public string FirstName { get; set; }
        [CsvColumn(FieldIndex = 2)]
        public string LastName { get; set; }
        [CsvColumn(FieldIndex = 3)]
        public string Address { get; set; }
        [CsvColumn(FieldIndex = 4)]
        public string PhoneNumber  {get; set; }

        public override string ToString()
        {
            return $"FirstName : {FirstName}, LastName = {LastName}";
        }
    }
}
