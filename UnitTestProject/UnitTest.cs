﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSVApp;
using FluentAssertions;
using CSVApp.Repository;

namespace UnitTestProject
{

    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestSortByFrequencyAndThenAlphabetically()
        {
            var repo = new IndividualRepository();
            var persistor = new TestPersistory();
            var domain = new SomeDomain(repo, persistor);

            domain.ExportSortedNamesByFrequencyThenAlphabetically();

            var array = persistor.Lines.ToArray();
            var first = array[0];
            first.Should().Be("Brown, 2");            

            var second = array[1];
            second.Should().Be("Clive, 2");            

            var last = array[8];
            last.Should().Be("John, 1");            
        }

        [TestMethod]
        public void TestSortByStreetName()
        {
            var repo = new IndividualRepository();
            var persistor = new TestPersistory();
            var domain = new SomeDomain(repo, persistor);

            domain.ExportSortedStreetName();

            var array = persistor.Lines.ToArray();
            var firstStreet = array[0];
            firstStreet.Should().Be("65 Ambling Way");            

            var secondStreet = array[1];
            secondStreet.Should().Be("8 Crimson Rd");            

            var lastStreet = array[7];
            lastStreet.Should().Be("49 Sutherland St");            
        }
    }
}
