﻿using CSVApp.Contract;
using System.Collections.Generic;

namespace UnitTestProject
{
    public class TestPersistory : IPersistor
    {
        public List<string> Lines { get; set; }
        
        public TestPersistory()
        {
            Lines = new List<string>();
        }

        public void WriteLine(string line)
        {
            Lines.Add(line);
        }
    }
}
